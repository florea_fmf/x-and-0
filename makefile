CC = gcc
CFLAGS = -c -Wall -Wextra -pedantic -Wmain -Wswitch-defaults -Wswitch-enum -Wmissing-include-dirs -Wmissing-declarations -Wunreachable-code -Winline -Wfloat-equal -Wundef -Wcast-align -Wredundant-decls -Winit-self -Wshadow -s -O2 -fomit-frame-pointer
LDFLAGS = -lSDL -lSDL_ttf -lSDL_gfx
SOURCES = src/main.c src/ai.c src/graphics.c src/logic.c
OBJECTS = $(SOURCES: .c=.o)
EXECUTABLE = release/x_and_0
RELEASEDIR = release


all: $(RELEASEDIR) $(SOURCES) $(EXECUTABLE)	

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(FLAGS) $< -o $@

$(RELEASEDIR) :
	mkdir -p $(RELEASEDIR) 

clean:
	rm -rf $(RELEASEDIR)

