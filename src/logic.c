/*
	 logic.c -- functions responsables for game logic
	 version 1.0, December 18th, 2012

	 Copyright (C) 2012 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
*/

#include "vars.h"
#include "logic.h"
#include "graphics.h"
#include "ai.h"
#include <stdlib.h>
#include <time.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_gfxPrimitives.h>


#define DELAY_BEFORE_END_STATE 2000		/* to let the user see the line that marks the winning move */


/* RGBA components used for creating images in memory */
#ifdef SDL_BIG_ENDIAN
	static uint32_t rmask = 0xff000000;
	static uint32_t gmask = 0x00ff0000;
	static uint32_t bmask = 0x0000ff00;
	static uint32_t amask = 0x000000ff;
#else
	static uint32_t rmask = 0x000000ff;
	static uint32_t gmask = 0x0000ff00;
	static uint32_t bmask = 0x00ff0000;
	static uint32_t amask = 0xff000000;
#endif


/** gets the square number where to put the X or 0 from the mouse position */
static int get_square(int *x, int *y);

/** checks to see if attempted move is valid */
static int valid_move(game_t *game);

/** marks a square in the grid as being occupied by a player */
static void mark_grid(game_t *game);

/** switches the players */
static void change_player(game_t *game);

/** puts either X or 0 in the square where the click event occured */
static void put_image_in_square(game_t *game);

/** draws the winning line starting at startingSquare up to endingSquare */
static void draw_winning_line(game_t *game, int startingSquare, int endingSquare);


int set_game_state(int state, game_t *game)
{
	SDL_Surface *image = NULL;
	SDL_Surface *textSurface = NULL;
	SDL_Color fontColor;
	int i;
	char *winner = malloc(sizeof(winner)*16);


	image = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_DOUBLEBUF,
								WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_BPP,
								rmask, gmask, bmask, amask);

	switch (state)
	{
		case STATE_MAIN_MENU :
			/* make the bg white */
			SDL_FillRect(image, &image->clip_rect, SDL_MapRGB(image->format, 0xff, 0xff, 0xff));

			/* put the game name */
			/* make it look shadowed */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "X and 0", fontColor);
			apply_surface(253, 111, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 106; fontColor.g = 90; fontColor.b = 205;		/* some kind of purple */
			textSurface = TTF_RenderText_Blended(game->font, "X and 0", fontColor);
			apply_surface(250, 110, textSurface, image);
			SDL_FreeSurface(textSurface);

			lineRGBA(image, 250, 150, 390, 150, 0x6a, 0x5a, 0xcd, 0xff);


			/* put the play button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Play", fontColor);
			apply_surface(253, 181, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 173; fontColor.g = 255; fontColor.b = 47;		/* green-yellow stuff */
			textSurface = TTF_RenderText_Blended(game->font, "Play", fontColor);
			apply_surface(250, 180, textSurface, image);
			SDL_FreeSurface(textSurface);


			/* put the difficulty button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Difficulty", fontColor);
			apply_surface(253, 221, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 30; fontColor.g = 144; fontColor.b = 255;		/* light-blue */
			textSurface = TTF_RenderText_Blended(game->font, "Difficulty", fontColor);
			apply_surface(250, 220, textSurface, image);
			SDL_FreeSurface(textSurface);


			/* put the exit button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Exit", fontColor);
			apply_surface(253, 261, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 255; fontColor.g = 0; fontColor.b = 0;		/* red */
			textSurface = TTF_RenderText_Blended(game->font, "Exit", fontColor);
			apply_surface(250, 260, textSurface, image);
			SDL_FreeSurface(textSurface);

			apply_surface(0, 0, image, game->screen);


			SDL_FreeSurface(image);

			break;
		case STATE_CHOOSE_PLAY :
			SDL_FillRect(image, &image->clip_rect, SDL_MapRGB(image->format, 0xff, 0xff, 0xff));

			/* put the "Play" title */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Play", fontColor);
			apply_surface(253, 111, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 173; fontColor.g = 255; fontColor.b = 47;
			textSurface = TTF_RenderText_Blended(game->font, "Play", fontColor);
			apply_surface(250, 110, textSurface, image);
			SDL_FreeSurface(textSurface);

			lineRGBA(image, 250, 150, 320, 150, 0xad, 0xff, 0x2f, 0xff);


			/* put the "Human" button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Human", fontColor);
			apply_surface(253, 181, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 30; fontColor.g = 144; fontColor.b = 255;
			textSurface = TTF_RenderText_Blended(game->font, "Human", fontColor);
			apply_surface(250, 180, textSurface, image);
			SDL_FreeSurface(textSurface);


			/* put the "Computer" button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Computer", fontColor);
			apply_surface(253, 221, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 255; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Computer", fontColor);
			apply_surface(250, 220, textSurface, image);
			SDL_FreeSurface(textSurface);


			/* put the "Back" button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Back", fontColor);
			apply_surface(253, 261, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 139; fontColor.g = 137; fontColor.b = 137;
			textSurface = TTF_RenderText_Blended(game->font, "Back", fontColor);
			apply_surface(250, 260, textSurface, image);
			SDL_FreeSurface(textSurface);

			apply_surface(0, 0, image, game->screen);


			SDL_FreeSurface(image);

			break;
		case STATE_CHOOSE_DIFFICULTY :
			SDL_FillRect(image, &image->clip_rect, SDL_MapRGB(image->format, 0xff, 0xff, 0xff));

			/* put the title "Difficulty" */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Difficulty", fontColor);
			apply_surface(253, 111, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 30; fontColor.g = 144; fontColor.b = 255;
			textSurface = TTF_RenderText_Blended(game->font, "Difficulty", fontColor);
			apply_surface(250, 110, textSurface, image);
			SDL_FreeSurface(textSurface);

			lineRGBA(image, 250, 150, 390, 150, 0x1e, 0x90, 0xff, 0xff);


			/* put the "Easy" option */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Easy", fontColor);
			apply_surface(253, 181, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 173; fontColor.g = 255; fontColor.b = 47;
			textSurface = TTF_RenderText_Blended(game->font, "Easy", fontColor);
			apply_surface(250, 180, textSurface, image);
			SDL_FreeSurface(textSurface);

			/* put the "Hard" option */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Hard", fontColor);
			apply_surface(253, 221, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 255; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Hard", fontColor);
			apply_surface(250, 220, textSurface, image);
			SDL_FreeSurface(textSurface);

			/* put the "Back" button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Back", fontColor);
			apply_surface(253, 261, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 139; fontColor.g = 137; fontColor.b = 137;
			textSurface = TTF_RenderText_Blended(game->font, "Back", fontColor);
			apply_surface(250, 260, textSurface, image);
			SDL_FreeSurface(textSurface);


			/* mark the selected item */
			if (game->difficulty == DIFFICULTY_EASY)
			{
				filledEllipseRGBA(image, 230, 200, 10, 10, 0xad, 0xff, 0x2f, 0xff);
			}
			else if (game->difficulty == DIFFICULTY_HARD)
			{
				filledEllipseRGBA(image, 230, 240, 10, 10, 0xff, 0x00, 0x00, 0xff);
			}

			apply_surface(0, 0, image, game->screen);


			SDL_FreeSurface(image);

			break;
		case STATE_GAME_PLAYING :
			for (i = 1; i <= 9; i++)
			{
				game->grid[i] = 0;
			}
			game->player = 1;
			game->winner = 0;
			game->ended = 0;

			apply_surface(0, 0, game->background, game->screen);

			SDL_FreeSurface(image);
			break;
		case STATE_GAME_END :
			SDL_FillRect(image, &image->clip_rect, SDL_MapRGB(image->format, 0xff, 0xff, 0xff));

			switch (game->winner)
			{
				case 1 : strcpy(winner, "Winner Player 1"); break;
				case 2 : strcpy(winner, "Winner Player 2"); break;
				case 3 : strcpy(winner, "Stalemate"); break;
				default : strcpy(winner, "dafuq?");
			}

			/* put the winner */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, winner, fontColor);
			apply_surface(213, 111, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 255; fontColor.g = 255; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, winner, fontColor);
			apply_surface(210, 110, textSurface, image);
			SDL_FreeSurface(textSurface);


			/* put the play again button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Play again", fontColor);
			apply_surface(253, 181, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 173; fontColor.g = 255; fontColor.b = 47;
			textSurface = TTF_RenderText_Blended(game->font, "Play again", fontColor);
			apply_surface(250, 180, textSurface, image);
			SDL_FreeSurface(textSurface);

			/* put the main menu button */
			fontColor.r = 0; fontColor.g = 0; fontColor.b = 0;
			textSurface = TTF_RenderText_Blended(game->font, "Main menu", fontColor);
			apply_surface(253, 221, textSurface, image);
			SDL_FreeSurface(textSurface);

			fontColor.r = 106; fontColor.g = 90; fontColor.b = 205;
			textSurface = TTF_RenderText_Blended(game->font, "Main menu", fontColor);
			apply_surface(250, 220, textSurface, image);
			SDL_FreeSurface(textSurface);


			apply_surface(0, 0, image, game->screen);


			SDL_FreeSurface(image);

			break;
		default :
			return 0;
	}


	free(winner);

	return 1;
}


void handle_mouse_event(game_t *game, int *x, int *y, int *click)
{
	/* check in what state the game is */
	switch (game->state)
	{
		case STATE_MAIN_MENU :
			/* a stupid solution, but gets the work done. Requires more cpu,
			   but less memory than using additional variables */
			if ((*x >= 250 && *x <= 320) && (*y >= 180 && *y <= 222))
			{
				/* play button
				   unmark previously selected item
				   unmark difficulty button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 410, 244, 455, 244, 0xff, 0xff, 0xff, 0xff);

				/* unmark exit button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 320, 284, 365, 284, 0xff, 0xff, 0xff, 0xff);


				/* mark play button as selected */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xad, 0xff, 0x2f, 0xff);
				lineRGBA(game->screen, 330, 200, 375, 200, 0xad, 0xff, 0x2f, 0xff);

				if (*click)
				{
					game->state = STATE_CHOOSE_PLAY;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else if ((*x >= 250 && *x <= 410) && (*y >= 224 && *y <= 262))
			{
				/* difficulty button
				   unmark play button */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 330, 200, 375, 200, 0xff, 0xff, 0xff, 0xff);

				/* unmark exit button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 320, 284, 365, 284, 0xff, 0xff, 0xff, 0xff);


				/* mark difficulty button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0x1e, 0x90, 0xff, 0xff);
				lineRGBA(game->screen, 410, 244, 455, 244, 0x1e, 0x90, 0xff, 0xff);

				if (*click)
				{
					game->state = STATE_CHOOSE_DIFFICULTY;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else if ((*x >= 250 && *x <= 320) && (*y >= 264 && *y <= 302))
			{
				/* exit button
				   unmark play button */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 330, 200, 375, 200, 0xff, 0xff, 0xff, 0xff);

				/* unmark difficulty button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 410, 244, 455, 244, 0xff, 0xff, 0xff, 0xff);


				/* mark exit button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0x00, 0x00, 0xff);
				lineRGBA(game->screen, 320, 284, 365, 284, 0xff, 0x00, 0x00, 0xff);

				if (*click)
				{
					game->exit = 1;
					*click = 0;
				}
			}
			else
			{
				/* unmark all buttons */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 330, 200, 375, 200, 0xff, 0xff, 0xff, 0xff);

				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 410, 244, 455, 244, 0xff, 0xff, 0xff, 0xff);

				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 320, 284, 365, 284, 0xff, 0xff, 0xff, 0xff);
			}
			break;
		case STATE_CHOOSE_PLAY :
			if ((*x >= 250 && *x <= 380) && (*y >= 180 && *y <= 222))
			{
				/* human button
				   unmark computer button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 430, 244, 475, 244, 0xff, 0xff, 0xff, 0xff);

				/* unmark back button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0xff, 0xff, 0xff, 0xff);


				/* mark human button */
				lineRGBA(game->screen, 200, 200, 245, 200, 0x1e, 0x90, 0xff, 0xff);
				lineRGBA(game->screen, 380, 200, 425, 200, 0x1e, 0x90, 0xff, 0xff);

				if (*click)
				{
					game->opponent = OPPONENT_HUMAN;
					game->state = STATE_GAME_PLAYING;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else if ((*x >= 250 && *x <= 430) && (*y >= 224 && *y <= 262))
			{
				/* computer button
				   unmark human button */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 380, 200, 425, 200, 0xff, 0xff, 0xff, 0xff);

				/* unmark back button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0xff, 0xff, 0xff, 0xff);


				/* mark computer button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0x00, 0x00, 0xff);
				lineRGBA(game->screen, 430, 244, 475, 244, 0xff, 0x00, 0x00, 0xff);

				if (*click)
				{
					game->opponent = OPPONENT_CPU;
					game->state = STATE_GAME_PLAYING;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else if ((*x >= 250 && *x <= 330) && (*y >= 264 && *y <= 302))
			{
				/* back button
				   unmark human button */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 380, 200, 425, 200, 0xff, 0xff, 0xff, 0xff);

				/* unmark computer button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 430, 244, 475, 244, 0xff, 0xff, 0xff, 0xff);


				/* mark back button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0x8b, 0x89, 0x89, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0x8b, 0x89, 0x89, 0xff);

				if (*click)
				{
					game->state = STATE_MAIN_MENU;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else
			{
				/* unmark all buttons */
				lineRGBA(game->screen, 200, 200, 220, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 380, 200, 425, 200, 0xff, 0xff, 0xff, 0xff);

				lineRGBA(game->screen, 200, 244, 220, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 430, 244, 475, 244, 0xff, 0xff, 0xff, 0xff);

				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0xff, 0xff, 0xff, 0xff);
			}
			break;
		case STATE_CHOOSE_DIFFICULTY :
			if ((*x >= 250 && *x <= 330) && (*y >= 180 && *y <= 222))
			{
				/* easy button
				   unmark hard button */
				lineRGBA(game->screen, 200, 244, 220, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 244, 385, 244, 0xff, 0xff, 0xff, 0xff);

				/* unmark back button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0xff, 0xff, 0xff, 0xff);


				/* mark easy button */
				lineRGBA(game->screen, 200, 200, 220, 200, 0xad, 0xff, 0x2f, 0xff);
				lineRGBA(game->screen, 340, 200, 385, 200, 0xad, 0xff, 0x2f, 0xff);

				if (*click)
				{
					game->difficulty = DIFFICULTY_EASY;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else if ((*x >= 250 && *x <= 330) && (*y >= 224 && *y <= 262))
			{
				/* hard button
				   unmark easy button */
				lineRGBA(game->screen, 200, 200, 220, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 200, 385, 200, 0xff, 0xff, 0xff, 0xff);

				/* unmark back button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0xff, 0xff, 0xff, 0xff);


				/* mark hard button */
				lineRGBA(game->screen, 200, 244, 220, 244, 0xff, 0x00, 0x00, 0xff);
				lineRGBA(game->screen, 340, 244, 385, 244, 0xff, 0x00, 0x00, 0xff);

				if (*click)
				{
					game->difficulty = DIFFICULTY_HARD;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else if ((*x >= 250 && *x <= 330) && (*y >= 264 && *y <= 302))
			{
				/* back button
				   unmark easy button */
				lineRGBA(game->screen, 200, 200, 220, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 200, 385, 200, 0xff, 0xff, 0xff, 0xff);

				/* unmark hard button */
				lineRGBA(game->screen, 200, 244, 220, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 244, 385, 244, 0xff, 0xff, 0xff, 0xff);


				/* mark back button */
				lineRGBA(game->screen, 200, 284, 245, 284, 0x8b, 0x89, 0x89, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0x8b, 0x89, 0x89, 0xff);

				if (*click)
				{
					game->state = STATE_MAIN_MENU;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else
			{
				/* unmark all buttons */
				lineRGBA(game->screen, 200, 200, 220, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 200, 385, 200, 0xff, 0xff, 0xff, 0xff);

				lineRGBA(game->screen, 200, 244, 220, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 244, 385, 244, 0xff, 0xff, 0xff, 0xff);

				lineRGBA(game->screen, 200, 284, 245, 284, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 340, 284, 385, 284, 0xff, 0xff, 0xff, 0xff);
			}
			break;
		case STATE_GAME_PLAYING :
			break;
		case STATE_GAME_END :
			if ((*x >= 250 && *x <= 432) && (*y >= 180 && *y <= 222))
			{
				/* play again button
				   unmark main menu button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 450, 244, 490, 244, 0xff, 0xff, 0xff, 0xff);


				/* mark play again button */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xad, 0xff, 0x2f, 0xff);
				lineRGBA(game->screen, 435, 200, 475, 200, 0xad, 0xff, 0x2f, 0xff);

				if (*click)
				{
					game->state = STATE_GAME_PLAYING;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else if ((*x >= 250 && *x <= 446) && (*y >= 224 && *y <= 262))
			{
				/* main menu button
				   unmark play again button */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 435, 200, 475, 200, 0xff, 0xff, 0xff, 0xff);


				/* mark main menu button */
				lineRGBA(game->screen, 200, 244, 245, 244, 0x6a, 0x5a, 0xcd, 0xff);
				lineRGBA(game->screen, 450, 244, 490, 244, 0x6a, 0x5a, 0xcd, 0xff);

				if (*click)
				{
					game->state = STATE_MAIN_MENU;
					set_game_state(game->state, game);
					*click = 0;
				}
			}
			else
			{
				/* unmark all buttons */
				lineRGBA(game->screen, 200, 200, 245, 200, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 435, 200, 475, 200, 0xff, 0xff, 0xff, 0xff);

				lineRGBA(game->screen, 200, 244, 245, 244, 0xff, 0xff, 0xff, 0xff);
				lineRGBA(game->screen, 450, 244, 490, 244, 0xff, 0xff, 0xff, 0xff);
			}
			break;
		default :
			printf("Should not get here\n");
	}
}


void make_move(game_t *game, int *x, int *y)
{
	game->square = get_square(x, y);

	if (valid_move(game))
	{
		mark_grid(game);
		put_image_in_square(game);
		change_player(game);
	}
}


static int get_square(int *x, int *y)
{
	if ((*x >= 0 && *x <= 210) && (*y >= 0 && *y <= 157))
	{
		return 1;
	}
	else if ((*x > 216 && *x <= 426) && (*y > 0 && *y <= 157))
	{
		return 2;
	}
	else if ((*x > 432 && *x <= 640) && (*y > 0 && *y <= 157))
	{
		return 3;
	}
	else if ((*x > 0 && *x <= 210) && (*y > 163 && *y <= 320))
	{
		return 4;
	}
	else if ((*x > 216 && *x <= 426) && (*y > 163 && *y <= 320))
	{
		return 5;
	}
	else if ((*x > 432 && *x <= 640) && (*y > 163 && *y <= 320))
	{
		return 6;
	}
	else if ((*x > 0 && *x <= 210) && (*y > 326 && *y <= 480))
	{
		return 7;
	}
	else if ((*x > 216 && *x <= 426) && (*y > 326 && *y <= 480))
	{
		return 8;
	}
	else if ((*x > 432 && *x <= 640) && (*y > 326 && *y <= 480))
	{
		return 9;
	}
	else
	{
		return 0;
	}
}


static int valid_move(game_t *game)
{
	if (game->grid[game->square] == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


static void mark_grid(game_t *game)
{
	game->grid[game->square] = game->player;
}


static void change_player(game_t *game)
{
	if (game->player == 1)
	{
		game->player = 2;
	}
	else if (game->player == 2)
	{
		game->player = 1;
	}
}


static void put_image_in_square(game_t *game)
{
	int x, y;


	switch (game->square)
	{
		case 1 : x = 0; y = 0; break;
		case 2 : x = 216; y = 0; break;
		case 3 : x = 432; y = 0; break;
		case 4 : x = 0; y = 163; break;
		case 5 : x = 216; y = 163; break;
		case 6 : x = 432; y = 163; break;
		case 7 : x = 0; y = 320; break;
		case 8 : x = 216; y = 320; break;
		case 9 : x = 432; y = 320; break;
		default : return ;
	}


	if (game->player == 1)
	{
		apply_surface(x, y, game->X, game->screen);
	}
	else if (game->player == 2)
	{
		apply_surface(x, y, game->O, game->screen);
	}
}


static void draw_winning_line(game_t *game, int startingSquare, int endingSquare)
{
	int j;


	if (startingSquare == 1 && endingSquare == 3)
	{
		for (j = 73; j <= 77; j++)
		{
			lineRGBA(game->screen, 40, j, 600, j, 0xff, 0xff, 0x00, 0xff);
		}
	}
	else if (startingSquare == 4 && endingSquare == 6)
	{
		for (j = 234; j <= 238; j++)
		{
			lineRGBA(game->screen, 40, j, 600, j, 0xff, 0xff, 0x00, 0xff);
		}
	}
	else if (startingSquare == 7 && endingSquare == 9)
	{
		for (j = 394; j <= 398; j++)
		{
			lineRGBA(game->screen, 40, j, 600, j, 0xff, 0xff, 0x00, 0xff);
		}
	}
	else if (startingSquare == 1 && endingSquare == 7)
	{
		for (j = 103; j <= 107; j++)
		{
			lineRGBA(game->screen, j, 40, j, 420, 0xff, 0xff, 0x00, 0xff);
		}
	}
	else if (startingSquare == 2 && endingSquare == 8)
	{
		for (j = 319; j <= 323; j++)
		{
			lineRGBA(game->screen, j, 40, j, 420, 0xff, 0xff, 0x00, 0xff);
		}
	}
	else if (startingSquare == 3 && endingSquare == 9)
	{
		for (j = 534; j <= 538; j++)
		{
			lineRGBA(game->screen, j, 40, j, 420, 0xff, 0xff, 0x00, 0xff);
		}
	}
	else if (startingSquare == 1 && endingSquare == 9)
	{
		for (j = 0; j <= 5; j++)
		{
			lineRGBA(game->screen, 40, j+40, 600, j+440, 0xff, 0xff, 0x00, 0xff);
		}
	}
	else if (startingSquare == 3 && endingSquare == 7)
	{
		for (j = 0; j <= 5; j++)
		{
			lineRGBA(game->screen, 40, j+440, 600, j+40, 0xff, 0xff, 0x00, 0xff);
		}
	}


	SDL_Flip(game->screen);
	SDL_Delay(DELAY_BEFORE_END_STATE);
}


int game_end(game_t *game)
{
	int i, total;


	/* check lines
	   for player 1 */
	if (game->grid[1] == 1 && game->grid[2] == 1 && game->grid[3] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 1, 3);
		return 1;
	}
	else if (game->grid[4] == 1 && game->grid[5] == 1 && game->grid[6] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 4, 6);
		return 1;
	}
	else if (game->grid[7] == 1 && game->grid[8] == 1 && game->grid[9] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 7, 9);
		return 1;
	}
	/* for player 2 */
	if (game->grid[1] == 2 && game->grid[2] == 2 && game->grid[3] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 1, 3);
		return 1;
	}
	else if (game->grid[4] == 2 && game->grid[5] == 2 && game->grid[6] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 4, 6);
		return 1;
	}
	else if (game->grid[7] == 2 && game->grid[8] == 2 && game->grid[9] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 7, 9);
		return 1;
	}

	/* check columns
	   for player 1 */
	if (game->grid[1] == 1 && game->grid[4] == 1 && game->grid[7] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 1, 7);
		return 1;
	}
	else if (game->grid[2] == 1 && game->grid[5] == 1 && game->grid[8] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 2, 8);
		return 1;
	}
	else if (game->grid[3] == 1 && game->grid[6] == 1 && game->grid[9] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 3, 9);
		return 1;
	}
	/* check for player 2 */
	if (game->grid[1] == 2 && game->grid[4] == 2 && game->grid[7] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 1, 7);
		return 1;
	}
	else if (game->grid[2] == 2 && game->grid[5] == 2 && game->grid[8] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 2, 8);
		return 1;
	}
	else if (game->grid[3] == 2 && game->grid[6] == 2 && game->grid[9] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 3, 9);
		return 1;
	}

	/* check diagonals
	   for player 1 */
	if (game->grid[1] == 1 && game->grid[5] == 1 && game->grid[9] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 1, 9);
		return 1;
	}
	else if (game->grid[3] == 1 && game->grid[5] == 1 && game->grid[7] == 1)
	{
		game->winner = 1;
		draw_winning_line(game, 3, 7);
		return 1;
	}
	/* for player 2 */
	if (game->grid[1] == 2 && game->grid[5] == 2 && game->grid[9] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 1, 9);
		return 1;
	}
	else if (game->grid[3] == 2 && game->grid[5] == 2 && game->grid[7] == 2)
	{
		game->winner = 2;
		draw_winning_line(game, 3, 7);
		return 1;
	}

	/* check for draw */
	for (i = 1, total = 0; i <= 9; i++)
	{
		if (game->grid[i] != 0)
		{
			total++;
		}
	}

	if (total == 9)
	{
		game->winner = 3;
		return 1;
	}


	return 0;
}


void cpu_make_move(game_t *game)
{
	if (game->difficulty == DIFFICULTY_EASY)
	{
		game->square = (rand() % 9) + 1;		/* random number [1..9] */
		while (!valid_move(game))
		{
			game->square = (rand() % 9) + 1;
		}
		mark_grid(game);
		put_image_in_square(game);
		change_player(game);
	}
	else if (game->difficulty == DIFFICULTY_HARD)
	{
		game->square = get_cpu_move(game);
		mark_grid(game);
		put_image_in_square(game);
		change_player(game);
	}
}
