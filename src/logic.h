/*
	 logic.h -- forward declarations and constants for game logic
	 version 1.0, December 18th, 2012

	 Copyright (C) 2012 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
*/

#ifndef LOGIC_H_INCLUDED
#define LOGIC_H_INCLUDED


#include "vars.h"
#include "graphics.h"


#define STATE_MAIN_MENU 1
#define STATE_GAME_PLAYING 2
#define STATE_GAME_END 3
#define STATE_CHOOSE_DIFFICULTY 4
#define STATE_CHOOSE_PLAY 5		/* choose against who to play */

#define DIFFICULTY_EASY 1		/* computer picks at random */
#define DIFFICULTY_HARD 2		/* computer has some strategy */

#define OPPONENT_HUMAN 1
#define OPPONENT_CPU 2

#define MARKER_X 1		/* what image to put in a square */
#define MARKER_0 2


/** changes the state of the game; returns succes/failure */
int set_game_state(int state, game_t *game);

/** handles mouse related events
	x, y == position where the mouse click occured
	click == 1 if click occured, 0 otherwise */
void handle_mouse_event(game_t *game, int *x, int *y, int *click);

/** puts X or 0 in a square */
void make_move(game_t *game, int *x, int *y);

/** checks to see if the game ended, one of the players won */
int game_end(game_t *game);

/** have the cpu make a move */
void cpu_make_move(game_t *game);

#endif
