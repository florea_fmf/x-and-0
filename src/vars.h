/*
	 vars.h -- data structures
	 version 1.0, December 18th, 2012

	 Copyright (C) 2012 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
*/

#ifndef VARS_H_INCLUDED
#define VARS_H_INCLUDED


#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>


typedef struct
{
	/** the current state for the game
		defined in logic.h */
	int state;

	/** true if the game has ended (one of the players has one
		or the game ended in a draw)
		has no correlation with user exiting the program */
	int ended;

	/** true if the user pressed the "exit" button, or the X on the window */
	int exit;

	/** the a.i. difficulty
		defined in logic.h */
	int difficulty;

	/** what type of opponent to face
		defined in logic.h */
	int opponent;

	/** who won the game
		1 == player1
		2 == player2
		3 == draw */
	int winner;

	/** contains the state of the game table
		0 == if empty
		1 == if player1 has occupied it
		2 == if player2 has occupied it */
	int grid[10];

	/** used to know in what square to put the X or 0 */
	int square;

	/** contains the number of the player next to make a move
		1 == for player1
		2 == for player2 */
	int player;


	/** main screen */
	SDL_Surface *screen;
	/** image for the grid */
	SDL_Surface *background;
	/** image for the X marker */
	SDL_Surface *X;
	/** image for the 0 marker */
	SDL_Surface *O;

	/** font used in game */
	TTF_Font *font;
	/** path to the font */
	char *fontPath;
} game_t;


#endif
