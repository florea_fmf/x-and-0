/*
	 graphics.h -- forward declarations and constants for game graphics
	 version 1.0, December 18th, 2012

	 Copyright (C) 2012 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
*/

#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED


#include <SDL/SDL.h>


#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480
#define WINDOW_BPP 32


/** applyies the image from source onto destination starting from x, y */
void apply_surface(int x, int y, SDL_Surface *source, SDL_Surface *destination);

/** generates and returns the background image, the grid , or NULL if smth went wrong */
SDL_Surface *get_background_image(void);

/** generates and returns the image for the X , or NULL if smth went wrong */
SDL_Surface *get_X_image(void);

/** generates and returns the image for the 0 , or NULL if smth went wrong */
SDL_Surface *get_O_image(void);


#endif
