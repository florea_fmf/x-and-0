/*
	 main.c -- main program (one source to rule them all)
	 version 1.0, December 18th, 2012

	 Copyright (C) 2012 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
*/

#include "vars.h"
#include "logic.h"
#include "graphics.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>


#define DELAY 100


/** initializes the game for first use
	returns succes/failure */
int init(game_t *game);

/** frees allocated resources */
void clean_up(game_t *game);

/** select a font to use by the program */
static char *get_font_path(void);


int main(int argc, char **argv)
{
	game_t game;
	SDL_Event event;
	int clickX, clickY, click; 		/* positions where the mouse button click occured */


	srand(time(NULL));

	if (!init(&game))
	{
		printf("Failed to initialize the game\n");
		return -1;
	}


	while (!game.exit)
	{
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
				case SDL_QUIT : game.exit = 1; break;
				case SDL_MOUSEBUTTONDOWN :
					click = 1;
					clickX = event.motion.x;
					clickY = event.motion.y;
					break;
				case SDL_MOUSEMOTION :
					click = 0;
					clickX = event.motion.x;
					clickY = event.motion.y;
					break;
				default :
					click = 0;
			}
		}

		handle_mouse_event(&game, &clickX, &clickY, &click);


		if (game.state == STATE_GAME_PLAYING)
		{
			if (game.opponent == OPPONENT_HUMAN && click)
			{	/* when playing against a human just loop this until game.end */
				make_move(&game, &clickX, &clickY);
				game.ended = game_end(&game);

				if (game.ended)
				{
					game.state = STATE_GAME_END;
					set_game_state(game.state, &game);
				}
			}
			else if (game.opponent == OPPONENT_CPU)
			{
				if (game.player == 1 && click)
				{	/* when playing against cpu, if is player1 turn then let the human make a move */
					make_move(&game, &clickX, &clickY);
					game.ended = game_end(&game);

					if (game.ended)
					{
						game.state = STATE_GAME_END;
						set_game_state(game.state, &game);
					}
				}
				else if (game.player == 2)
				{	/* it it is player2 move, then have the cpu make a move */
					cpu_make_move(&game);
					game.ended = game_end(&game);

					if (game.ended)
					{
						game.state = STATE_GAME_END;
						set_game_state(game.state, &game);
					}
				}
			}
		}


		if (SDL_Flip(game.screen) == -1)
		{
			printf("Failed to update the screen\n");
			game.exit = 2;
		}


		SDL_Delay(DELAY);
	}


	clean_up(&game);

	if (game.exit == 2)
	{
		return -1;
	}

	return 0;
}


char *get_font_path(void)
{
	#ifdef __unix__
		if (access("/usr/share/fonts/TTF/DejaVuSans.ttf", F_OK) == 0)
		{
			return "/usr/share/fonts/TTF/DejaVuSans.ttf";
		}
		else if (access("/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf", F_OK) == 0)
		{
			return "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf";
		}
		else
		{
			return "\0";
		}
	#else
		return "C:\\Windows\\Fonts\\Arial.ttf";
	#endif
}


int init(game_t *game)
{
	int i;


	/* initialize the game variables */
	game->state = 0;
	game->difficulty = 0;
	game->ended = 0;
	game->exit = 0;
	game->winner = 0;
	game->square = 0;
	game->player = 0;
	game->screen = NULL;
	game->background = NULL;
	game->X = NULL;
	game->O = NULL;
	game->fontPath = malloc(sizeof(game->fontPath)*255);
	game->font = NULL;
	for (i = 1; i <= 9; i++)
	{
		game->grid[i] = 0;
	}


	/* try to init SDL stuff */
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		return 0;
	}

	if (TTF_Init() == -1)
	{
		return 0;
	}

	SDL_WM_SetCaption("X and 0", NULL);


	/* set the game vars */
	game->state = STATE_MAIN_MENU;
	game->difficulty = DIFFICULTY_EASY;
	game->player = 1;
	game->screen = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_BPP,
									SDL_HWSURFACE | SDL_DOUBLEBUF);
	if (game->screen == NULL)
	{
		return 0;
	}

	game->background = get_background_image();
	if (game->background == NULL)
	{
		return 0;
	}

	game->X = get_X_image();
	if (game->X == NULL)
	{
		return 0;
	}

	game->O = get_O_image();
	if (game->O == NULL)
	{
		return 0;
	}

	strcpy(game->fontPath, get_font_path());
	game->font = TTF_OpenFont(game->fontPath, 35);
	if (game->font == NULL)
	{
		return 0;
	}


	if (!set_game_state(game->state, game))
	{
		return 0;
	}


	return 1;
}


void clean_up(game_t *game)
{
	if (game->background != NULL)
	{
		SDL_FreeSurface(game->background);
	}

	if (game->X != NULL)
	{
		SDL_FreeSurface(game->X);
	}

	if (game->O != NULL)
	{
		SDL_FreeSurface(game->O);
	}

	free(game->fontPath);
	if (game->font != NULL)
	{
		TTF_CloseFont(game->font);
	}

	if (game->screen != NULL)
	{
		SDL_FreeSurface(game->screen);
	}


	TTF_Quit();
	SDL_Quit();
}
