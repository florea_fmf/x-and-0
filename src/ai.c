/*
	 ai.c -- functions responsables for game ai
	 version 1.0, December 18th, 2012

	 Copyright (C) 2012 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
*/

#include "ai.h"


#define inf 1 << 20		/* infinite */


static int posmin;		/* what position should O move */
static int posmax;		/* what position should X move, not needed here but might be in the future */


/** checks to see if the table is won and by who */
static int check_win(game_t *game);
/** checks to see if the really ended in a draw */
static int check_draw(game_t *game);
/** the minimax algorithm */
static int minimax(int player, game_t *game, int n);


int get_cpu_move(game_t *game)
{
	posmin = posmax = -1;

	if (game->player == 1)
	{
		minimax(1, game, 0);
		return posmax;
	}
	else
	{
		minimax(2, game, 0);
		return posmin;
	}
}



static int check_win(game_t *game)
{
	if ((game->grid[1] == 1 && game->grid[2] == 1 && game->grid[3] == 1) ||
		(game->grid[4] == 1 && game->grid[5] == 1 && game->grid[6] == 1) ||
		(game->grid[7] == 1 && game->grid[8] == 1 && game->grid[9] == 1) ||
		(game->grid[1] == 1 && game->grid[4] == 1 && game->grid[7] == 1) ||
		(game->grid[2] == 1 && game->grid[5] == 1 && game->grid[8] == 1) ||
		(game->grid[3] == 1 && game->grid[6] == 1 && game->grid[9] == 1) ||
		(game->grid[1] == 1 && game->grid[5] == 1 && game->grid[9] == 1) ||
		(game->grid[3] == 1 && game->grid[5] == 1 && game->grid[7] == 1))
	{
		/* X wins */
		return 1;
	}
	else if ((game->grid[1] == 2 && game->grid[2] == 2 && game->grid[3] == 2) ||
			 (game->grid[4] == 2 && game->grid[5] == 2 && game->grid[6] == 2) ||
			 (game->grid[7] == 2 && game->grid[8] == 2 && game->grid[9] == 2) ||
			 (game->grid[1] == 2 && game->grid[4] == 2 && game->grid[7] == 2) ||
			 (game->grid[2] == 2 && game->grid[5] == 2 && game->grid[8] == 2) ||
			 (game->grid[3] == 2 && game->grid[6] == 2 && game->grid[9] == 2) ||
			 (game->grid[1] == 2 && game->grid[5] == 2 && game->grid[9] == 2) ||
			 (game->grid[3] == 2 && game->grid[5] == 2 && game->grid[7] == 2))
	{
		/* O wins */
		return -1;
	}
	else
	{
		/* draw */
		return 0;
	}
}


static int check_draw(game_t *game)
{
	if ((check_win(game) == 0) && (game->grid[1] != 0) && (game->grid[2] != 0) &&
		 (game->grid[3] != 0) && (game->grid[4] != 0) && (game->grid[5] != 0) &&
		 (game->grid[6] != 0) && (game->grid[7] != 0) && (game->grid[8] != 0) &&
		 (game->grid[9] != 0))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


static int minimax(int player, game_t *game, int n)
{
	int i, res;
	int max = -inf;
	int min = inf;
	int check = check_win(game);


	if (check == 1)
	{
		return 1;
	}
	else if (check == -1)
	{
		return -1;
	}
	else if (check != check_draw(game))
	{
		return 0;
	}


	for (i = 1; i <= 9; i++)
	{
		if (game->grid[i] == 0)
		{
			if (player == 2)
			{
				game->grid[i] = 2;
				res = minimax(1, game, n+1);

				game->grid[i] = 0;
				if (res < min)
				{
					min = res;
					if (n == 0)
					{
						posmin = i;
					}
				}
			}
			else if (player == 1)
			{
				game->grid[i] = 1;
				res = minimax(2, game, n+1);

				game->grid[i] = 0;
				if (res > max)
				{
					max = res;
					if (n == 0)
					{
						posmax = i;
					}
				}
			}
		}
	}


	if (player == 1)
	{
		return max;
	}
	else
	{
		return min;
	}
}
