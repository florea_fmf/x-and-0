/*
	 graphics.c -- functions responsables for game graphic
	 version 1.0, December 18th, 2012

	 Copyright (C) 2012 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
*/

#include "graphics.h"
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>


#ifdef SDL_BIG_ENDIAN
	static uint32_t rmask = 0xff000000;
	static uint32_t gmask = 0x00ff0000;
	static uint32_t bmask = 0x0000ff00;
	static uint32_t amask = 0x000000ff;
#else
	static uint32_t rmask = 0x000000ff;
	static uint32_t gmask = 0x0000ff00;
	static uint32_t bmask = 0x00ff0000;
	static uint32_t amask = 0xff000000;
#endif


void apply_surface(int x, int y, SDL_Surface *source, SDL_Surface *destination)
{
	SDL_Rect offset;


	offset.x = x;
	offset.y = y;

	SDL_BlitSurface(source, NULL, destination, &offset);
}


SDL_Surface *get_background_image(void)
{
	SDL_Surface *image = NULL;
	int i;


	image = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_DOUBLEBUF,
								WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_BPP,
								rmask, gmask, bmask, amask);
	if (image == NULL)
	{
		return NULL;
	}

	/* make the bg white */
	SDL_FillRect(image, &image->clip_rect, SDL_MapRGB(image->format, 0xff, 0xff, 0xff));

	/* draw the lines making the grid */
	for (i = 210; i <= 216; i++)
	{
		lineRGBA(image, i, 0, i, WINDOW_HEIGHT, 0x00, 0x00, 0x00, 0xff);
		lineRGBA(image, i+213, 0, i+213, WINDOW_HEIGHT, 0x00, 0x00, 0x00, 0xff);
	}

	for (i = 157; i <= 163; i++)
	{
		lineRGBA(image, 0, i, WINDOW_WIDTH, i, 0x00, 0x00, 0x00, 0xff);
		lineRGBA(image, 0, i+160, WINDOW_WIDTH, i+160, 0x00, 0x00, 0x00, 0xff);
	}


	return image;
}


SDL_Surface *get_X_image(void)
{
	SDL_Surface *image = NULL;
	int i;


	image = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_DOUBLEBUF,
								210, 157, WINDOW_BPP,
								rmask, gmask, bmask, amask);
	if (image == NULL)
	{
		return NULL;
	}

	for (i = 27; i <= 33; i++)
	{
		lineRGBA(image, 30, i, 180, i+90, 0x00, 0x00, 0xff, 0xff);
		lineRGBA(image, 180, i, 30, i+90, 0x00, 0x00, 0xff, 0xff);
	}

	return image;
}


SDL_Surface *get_O_image(void)
{
	SDL_Surface *image = NULL;
	int i;


	image = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_DOUBLEBUF,
								210, 157, WINDOW_BPP,
								rmask, gmask, bmask, amask);
	if (image == NULL)
	{
		return NULL;
	}

	for (i = 1; i <= 6; i++)
	{
		ellipseRGBA(image, 105, 78, i+84, i+57, 0xff, 0x00, 0x00, 0xff);
	}


	return image;
}
