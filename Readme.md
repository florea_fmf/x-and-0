X and 0 (a tic-tac-toe game)
============================

---
Info
---
	*A tic-tac-toe game
	*Works on unix-like systems and windows
	*Requires SDL, SDL_ttf, SDL_gfx (on all platforms)

---

Compile
-------
	*On linux: use the make file or the codeblocks project file
	*On windows: use the codeblocks project file with the step:
		- add: "-mwindows -lmingw32 -lSDLmain" in project build options before "-lSDL"

---

Disclaimer
----------
	* I have made this game after reading through the tutorials at lazy foo (http://lazyfoo.net/SDL_tutorials/index.php)

---

Pics or it didn't happen
------------------------
	http://imgur.com/a/aLOk2

